/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _calculator = __webpack_require__(1);

	var _calculator2 = _interopRequireDefault(_calculator);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var App = function () {
	    function App() {
	        _classCallCheck(this, App);

	        this.calculatorController = new _calculator2.default();
	        document.addEventListener("click", function () {
	            alert('asdasdasd');
	        });
	        alert('wyszlo');
	    }

	    _createClass(App, [{
	        key: 'calculateClick',
	        value: function calculateClick() {
	            this.calculatorController.setIpStart(document.getElementById("start"));
	            this.calculatorController.setIpEnd(document.getElementById("end"));
	        }
	    }, {
	        key: 'testMethod',
	        value: function testMethod() {
	            console.log(true);
	        }
	    }]);

	    return App;
	}();

	var app = new App();

/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var IpCalculator = exports.IpCalculator = function () {
	    function IpCalculator() {
	        _classCallCheck(this, IpCalculator);

	        this.ipStart = false;
	        this.ipEnd = false;
	    }

	    _createClass(IpCalculator, [{
	        key: 'setIpStart',
	        value: function setIpStart(ipStart) {
	            this.ipStart = ipStart;
	        }
	    }, {
	        key: 'setIpEnd',
	        value: function setIpEnd(ipEnd) {
	            this.ipEnd = ipEnd;
	        }
	    }, {
	        key: 'calculate',
	        value: function calculate() {
	            var ipStartNum, ipEndNum, ipCurNum;
	            var rangeCollection = [];
	            try {
	                ipStartNum = this.toDecimal(this.ipStart);
	                ipEndNum = this.toDecimal(this.ipEnd);
	            } catch (err) {
	                return null;
	            }
	            if (ipEndNum < ipStartNum) {
	                return null;
	            }
	            ipCurNum = ipStartNum;
	            while (ipCurNum <= ipEndNum) {
	                var optimalRange = this.getOptimalRange(ipCurNum, ipEndNum);
	                if (optimalRange === null) {
	                    return null;
	                }
	                rangeCollection.push(optimalRange);
	                ipCurNum = optimalRange.ipHigh + 1;
	            }
	            return rangeCollection;
	        }
	    }, {
	        key: 'calculateSubnetMask',
	        value: function calculateSubnetMask(ip, prefixSize) {
	            var ipNum;
	            try {
	                ipNum = this.toDecimal(ip);
	            } catch (err) {
	                return null;
	            }
	            return this.getMaskRange(ipNum, prefixSize);
	        }
	    }, {
	        key: 'getOptimalRange',
	        value: function getOptimalRange(ipNum, ipEndNum) {
	            var prefixSize,
	                optimalRange = null;
	            for (prefixSize = 32; prefixSize >= 0; prefixSize--) {
	                var maskRange = this.getMaskRange(ipNum, prefixSize);
	                if (maskRange.ipLow === ipNum && maskRange.ipHigh <= ipEndNum) {
	                    optimalRange = maskRange;
	                } else {
	                    break;
	                }
	            }
	            return optimalRange;
	        }
	    }, {
	        key: 'getMaskRange',
	        value: function getMaskRange(ipNum, prefixSize) {
	            var prefixMask = this.getPrefixMask(prefixSize);
	            var lowMask = this.getMask(32 - prefixSize);
	            var ipLow = (ipNum & prefixMask) >>> 0;
	            var ipHigh = ((ipNum & prefixMask) >>> 0) + lowMask >>> 0;
	            return {
	                ipLow: ipLow,
	                ipLowStr: this.toString(ipLow),

	                ipHigh: ipHigh,
	                ipHighStr: this.toString(ipHigh),

	                prefixMask: prefixMask,
	                prefixMaskStr: this.toString(prefixMask),
	                prefixSize: prefixSize,

	                invertedMask: lowMask,
	                invertedMaskStr: this.toString(lowMask),
	                invertedSize: 32 - prefixSize
	            };
	        }
	    }, {
	        key: 'getPrefixMask',
	        value: function getPrefixMask(prefixSize) {
	            var mask = 0;
	            var i;
	            for (i = 0; i < prefixSize; i++) {
	                mask += 1 << 32 - (i + 1) >>> 0;
	            }
	            return mask;
	        }
	    }, {
	        key: 'getMask',
	        value: function getMask(maskSize) {
	            var mask = 0;
	            var i;
	            for (i = 0; i < maskSize; i++) {
	                mask += 1 << i >>> 0;
	            }
	            return mask;
	        }
	    }, {
	        key: 'toDecimal',
	        value: function toDecimal(ipString) {
	            if (typeof ipString === 'number' && this.isDecimalIp(ipString) === true) {
	                return ipString;
	            }

	            if (this.isIp(ipString) === false) {
	                throw new Error('Not an IP address: ' + ipString);
	            }

	            var d = ipString.split('.');

	            return ((+d[0] * 256 + +d[1]) * 256 + +d[2]) * 256 + +d[3];
	        }
	    }, {
	        key: 'isIp',
	        value: function isIp(ip) {
	            if (typeof ip !== 'string') {
	                return false;
	            }
	            var parts = ip.match(/^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$/);
	            if (parts === null) {
	                return false;
	            }
	            for (var i = 1; i <= 4; i++) {
	                var n = parseInt(parts[i], 10);

	                if (n > 255 || n < 0) {
	                    return false;
	                }
	            }

	            return true;
	        }
	    }, {
	        key: 'isDecimalIp',
	        value: function isDecimalIp(ipNum) {
	            return typeof ipNum === 'number' && // is this a number?
	            ipNum % 1 === 0 && // does the number have a decimal place?
	            ipNum >= 0 && ipNum <= 4294967295;
	        }
	    }]);

	    return IpCalculator;
	}();

/***/ }
/******/ ]);