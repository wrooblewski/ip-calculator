export class IpCalculator {
    constructor() {
        this.ipStart = false;
        this.ipEnd = false;
    }

    setIpStart(ipStart) {
        this.ipStart = ipStart;
    }

    setIpEnd(ipEnd) {
        this.ipEnd = ipEnd;
    }

    calculate() {
        var ipStartNum, ipEndNum, ipCurNum;
        var rangeCollection = [];
        try {
            ipStartNum = this.toDecimal(this.ipStart);
            ipEndNum = this.toDecimal(this.ipEnd);
        }
        catch (err) {
            return null;
        }
        if (ipEndNum < ipStartNum) {
            return null;
        }
        ipCurNum = ipStartNum;
        while (ipCurNum <= ipEndNum) {
            var optimalRange = this.getOptimalRange(ipCurNum, ipEndNum);
            if (optimalRange === null) {
                return null;
            }
            rangeCollection.push(optimalRange);
            ipCurNum = optimalRange.ipHigh + 1;
        }
        return rangeCollection;
    }

    calculateSubnetMask(ip, prefixSize) {
        var ipNum;
        try {
            ipNum = this.toDecimal(ip);
        }
        catch (err) {
            return null;
        }
        return this.getMaskRange(ipNum, prefixSize);
    }

    getOptimalRange(ipNum, ipEndNum) {
        var prefixSize,
            optimalRange = null;
        for (prefixSize = 32; prefixSize >= 0; prefixSize--) {
            var maskRange = this.getMaskRange(ipNum, prefixSize);
            if ((maskRange.ipLow === ipNum) && (maskRange.ipHigh <= ipEndNum)) {
                optimalRange = maskRange;
            } else {
                break;
            }
        }
        return optimalRange;
    }

    getMaskRange(ipNum, prefixSize) {
        var prefixMask = this.getPrefixMask(prefixSize);
        var lowMask = this.getMask(32 - prefixSize);
        var ipLow = ( ipNum & prefixMask ) >>> 0;
        var ipHigh = (((ipNum & prefixMask ) >>> 0 ) + lowMask ) >>> 0;
        return {
            ipLow: ipLow,
            ipLowStr: this.toString(ipLow),

            ipHigh: ipHigh,
            ipHighStr: this.toString(ipHigh),

            prefixMask: prefixMask,
            prefixMaskStr: this.toString(prefixMask),
            prefixSize: prefixSize,

            invertedMask: lowMask,
            invertedMaskStr: this.toString(lowMask),
            invertedSize: 32 - prefixSize
        };
    }

    getPrefixMask(prefixSize) {
        var mask = 0;
        var i;
        for (i = 0; i < prefixSize; i++) {
            mask += (1 << (32 - (i + 1))) >>> 0;
        }
        return mask;
    }

    getMask(maskSize) {
        var mask = 0;
        var i;
        for (i = 0; i < maskSize; i++) {
            mask += ( 1 << i ) >>> 0;
        }
        return mask;
    }

    toDecimal(ipString) {
        if (( typeof ipString === 'number' ) && ( this.isDecimalIp(ipString) === true )) {
            return ipString;
        }

        if (this.isIp(ipString) === false) {
            throw new Error('Not an IP address: ' + ipString);
        }

        var d = ipString.split('.');

        return ( ( ( ( ( ( +d[0] ) * 256 ) + ( +d [1] ) ) * 256 ) + ( +d[2] ) ) * 256 ) + ( +d[3] );
    }

    isIp(ip) {
        if (typeof ip !== 'string') {
            return false;
        }
        var parts = ip.match(/^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$/);
        if (parts === null) {
            return false;
        }
        for (var i = 1; i <= 4; i++) {
            var n = parseInt(parts[i], 10);

            if (( n > 255 ) || ( n < 0 )) {
                return false;
            }
        }

        return true;
    }

    isDecimalIp (ipNum) {
        return (
            ( typeof ipNum === 'number' ) && // is this a number?
            ( ipNum % 1 === 0 ) && // does the number have a decimal place?
            ( ipNum >= 0 ) &&
            ( ipNum <= 4294967295 )
        );
    }

}